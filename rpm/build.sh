#!/bin/bash

set -euo pipefail

. /etc/os-release

ARG0=${0##*/}
ARCH=x86_64
OS="${ID}"
OS_VERSION="${VERSION_ID}"
RPM_DIST=$(/usr/lib/rpm/redhat/dist.sh)
RPM_DIST=${RPM_DIST#.}
SPEC_FILE="rpm/lens.spec"

usage() {
  cat << __END

  Build RPM using mock. Run from the root of the source directory for the
  package to be built.

  By default the RPM is built for the OS defined in /etc/os-release.

  The resulting RPMs will be available in the mock result directory.

  usage: $ARG0 [OPTION]

  Options:

  -a, --arch       Override the target architecture for the RPM.
  --os             Override the target OS for the RPM.
  --os-version     Override the target OS Version for the RPM.
  -d, --dist       Override the target RPM distribution.
  -s, --spec       Specify the path to the spec file to build.

  Examples:

  To build for Fedora 30 x86_64 use:

  ${ARG0} --arch x86_64 \\
    --os fedora \\
    --os-version 30 \\
    --dist fc30 \\
    --spec rpm/lens.spec

__END

  exit "${1}"
}

declare -r long_opts="arch:,os:,os-version:,dist:,spec:,help"
declare -r short_opts="a:d:s:h"

if ! parsed_options=$(getopt --options "${short_opts}" --long "${long_opts}" -- "$@"); then
  usage 1
fi

eval set -- "${parsed_options}"

while [ "$1" != '--' ]
do
  case "$1" in
    -a|--arch)      ARCH=$2;         shift 2 ;;
    --os)           OS=$2;           shift 2 ;;
    --os-version)   OS_VERSION=$2;   shift 2 ;;
    -d|--dist)      RPM_DIST=$2;     shift 2 ;;
    -s|--spec)      SPEC_FILE=$2;    shift 2 ;;
    -h|--help)      usage 0;         shift   ;;
    *) echo "Illegal option $1" >&2
       usage 1 ;;
  esac
done

RPM_NAME=$(awk '/Name:/{print $2}' "${SPEC_FILE}")
RPM_VERSION=$(awk '/Version:/{print $2}' "${SPEC_FILE}")
RPM_RELEASE=$(awk '/Release:/{sub("%{\\?dist}", ""); print $2}' "${SPEC_FILE}")

tar \
  --exclude-vcs \
  --exclude=".gitignore" \
  --exclude="*.tar.gz" \
  --exclude="${SPEC_FILE}" \
  -hczvf "${RPM_NAME}-${RPM_VERSION}.tar.gz" \
  --transform "s,^,${RPM_NAME}-${RPM_VERSION}/," \
  -- *

mock --root "${OS}-${OS_VERSION}-${ARCH}" \
  --buildsrpm --spec "${SPEC_FILE}" \
  --sources="${RPM_NAME}-${RPM_VERSION}.tar.gz"

mock --no-clean --root "${OS}-${OS_VERSION}-${ARCH}" \
  --rebuild "/var/lib/mock/${OS}-${OS_VERSION}-${ARCH}/result/${RPM_NAME}-${RPM_VERSION}-${RPM_RELEASE}.${RPM_DIST}.src.rpm"

ls -l "/var/lib/mock/${OS}-${OS_VERSION}-${ARCH}/result/"*.rpm
