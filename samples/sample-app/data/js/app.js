var app = angular.module('lens-app', ['lens.bridge', 'lens.ui']);

angular.element(document).ready(function () {
  angular.bootstrap(document, ['lens-app']);
});

app.controller('AppCtrl', function($scope, $timeout, $lensModal) {
  /* MODEL */
  var vm = angular.extend(this, {
    hostname: 'unknown',
    foo: true,
    bar: 'green',
    items: ['item1', 'item2', 'item3'],
    progress: 0,
    maximizeButtonText: "Maximize",
    fullscreenButtonText: "Fullscreen",
  });

  /* METHODS */
  angular.extend(vm, {
    closeApp: closeApp,
    fullscreen: fullscreen,
    maximize: maximize,
    openModal: openModal,
    updateHostname: updateHostname
  });

  /* SIGNALS */
  $scope.$on('update-config', _onUpdateConfig);
  $scope.$on('window-maximized', _onWindowMaximized);
  $scope.$on('window-unmaximized', _onWindowUnmaximized);
  $scope.$on('window-fullscreen', _onWindowFullscreen);
  $scope.$on('window-unfullscreen', _onWindowUnfullscreen);

  activate();

  ////////

  function activate() {
    $scope.emit('get-hostname');
    _updateProgress();
  }

  function closeApp() {
    $scope.emit('close');
  };

  function fullscreen() {
    $scope.emit('toggle-window-fullscreen');
  }

  function maximize() {
    $scope.emit('toggle-window-maximize');
  }

  function openModal() {
    console.log('Opening modal');

    var modalInstance = $lensModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      size: 'sm',
      resolve: {
        items: function () {
          return vm.items;
        }
      }
    });

    modalInstance.result.then(function(selectedItem) {
      console.log('Selected: ' + selectedItem);
    }, function () {
      console.info('Modal dismissed at: ' + new Date());
    });
  }

  function updateHostname() {
    $scope.emit('update-hostname', vm.hostname);
  }

  /* PRIVATE */

  function _updateProgress() {
    vm.progress = Math.floor(Math.random() * 100);
    $timeout(_updateProgress, 3000);
  }

  function _onUpdateConfig(e, hostname) {
    vm.hostname = hostname;
  }

  function _onWindowFullscreen(e) {
    vm.fullscreenButtonText = "Exit Fullscreen";
  }

  function _onWindowMaximized(e) {
    vm.maximizeButtonText = "Unmaximize";
  }

  function _onWindowUnfullscreen(e) {
    vm.fullscreenButtonText = "Fullscreen";
  }

  function _onWindowUnmaximized(e) {
    vm.maximizeButtonText = "Maximize";
  }
});

app.controller('ModalInstanceCtrl', function ($lensModalInstance, items) {
  var $ctrl = this;
  $ctrl.items = items;
  $ctrl.selected = {
    item: $ctrl.items[0]
  };

  $ctrl.ok = function () {
    $lensModalInstance.close($ctrl.selected.item);
  };

  $ctrl.cancel = function () {
    $lensModalInstance.dismiss('cancel');
  };
});
