#!/bin/bash

ANGULAR_VERSION="1.7.8"
BOOTSTRAP_VERSION="4.3.1"
JQUERY_VERSION="3.4.1"

echo "Updating AngularJS ..."
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular.js -O ./lens-data/js/angular.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular.min.js -O ./lens-data/js/angular.min.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular.min.js.map -O ./lens-data/js/angular.min.js.map

wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-animate.js -O ./lens-data/js/angular-animate.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-animate.min.js -O ./lens-data/js/angular-animate.min.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-animate.min.js.map -O ./lens-data/js/angular-animate.min.js.map

wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-route.js -O ./lens-data/js/angular-route.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-route.min.js -O ./lens-data/js/angular-route.min.js
wget https://code.angularjs.org/${ANGULAR_VERSION}/angular-route.min.js.map -O ./lens-data/js/angular-route.min.js.map


echo "Updating JQuery ..."
wget https://code.jquery.com/jquery-${JQUERY_VERSION}.js -O ./lens-data/js/jquery.js
wget https://code.jquery.com/jquery-${JQUERY_VERSION}.min.js -O ./lens-data/js/jquery.min.js
wget https://code.jquery.com/jquery-${JQUERY_VERSION}.min.map -O ./lens-data/js/jquery.min.js.map

echo "Updating Bootstrap ..."
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/css/bootstrap.css -O ./lens-data/css/bootstrap.css
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/css/bootstrap.min.css -O ./lens-data/css/bootstrap.min.css
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/css/bootstrap.min.css.map -O ./lens-data/css/bootstrap.min.css.map

wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/js/bootstrap.bundle.js -O ./lens-data/js/bootstrap.bundle.js
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/js/bootstrap.bundle.js.map -O ./lens-data/js/bootstrap.bundle.js.map
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/js/bootstrap.bundle.min.js -O ./lens-data/js/bootstrap.bundle.min.js
wget https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/${BOOTSTRAP_VERSION}/js/bootstrap.bundle.min.js.map -O ./lens-data/js/bootstrap.bundle.min.js.map
